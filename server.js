var isDev = !!process.argv[2];

if (isDev) {
  console.log('Running development server');
} else {
  console.log('Running production server');
}

//START WEBSERVER DATALYSE
var express = require('express');
var fs = require('fs');
var app = express();

app.use(express.static('website'));

app.get('/', function (req, res) {
   res.sendFile('index.html');
});

app.get(/^(.+)$/, function(req, res){
    //console.log('static file request : '+req.params[0]);
    if (fs.existsSync(req.params[0])) {
        res.sendfile(req.params[0]);
    }else{//the file not exists
        res.sendfile('website/index.html')
    }
});

var port_http = isDev ? '8080' : '80';
var ip_http = isDev ? 'localhost' : '93.93.68.140';
app.listen(port_http, ip_http, function () {
  console.log(`Datalyse app listening on port ${port_http}!`);
});
//END WEBSERVER DATALYSE

//START MONGODB SQL

var MongoClient = require('mongodb').MongoClient;

// Connect to the db
var mongoURL = isDev ? '77.81.116.239' : 'localhost';
MongoClient.connect(`mongodb://${mongoURL}:27017/datalysedb`, function(err, db) {
  var dbo = db.db("datalysedb");
  if (err) {
    throw err;
  }
  console.log("We are connected");
  //INIT start websockets and more process if never started after

  //START SOCKETS
  var WebSocketServer = require('ws').Server;
  var cfg = isDev ? {
    ssl: false,
    port: 8000
  } : {
    ssl: true,
    port: 8222,
    ssl_key: '/furanet/sites/app.datalyse.co/web/certificate.key',
    ssl_cert: '/furanet/sites/app.datalyse.co/web/certificate.crt'
  };

  var httpServ = (cfg.ssl) ? require('https') : require('http');
  var app = null;
  var processRequest = function(req, res) {
    res.writeHead(200);
    res.end("Enter to datalyse.co\n");
  };

  if (cfg.ssl) {
    app = httpServ.createServer({
        key: fs.readFileSync(cfg.ssl_key),
        cert: fs.readFileSync(cfg.ssl_cert)
      }, processRequest).listen(cfg.port);
  } else {
    app = httpServ.createServer(processRequest).listen(cfg.port);
  }

  // console.log(app);

  // Socket tools
  var onsocks = {};
  var onsocksvalidos = [];

  function socket_on(nombre,callback) {
    onsocksvalidos.push(nombre);
    onsocks[nombre] = callback;
  };

  function socket_emit() {
    var datas = [];
    var sockete = arguments[0];
    if (sockete) {
      datas.push(arguments[1]);
      if (arguments.length>1) {
        for (var i=2; i<arguments.length; i++) {
          datas.push(arguments[i]);
        }
      }
      if (sockete && sockete.readyState && sockete.readyState == 1) {
        sockete.send(JSON.stringify(datas));
      } //abierto
    }
  };

  function IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  function onmensaje(socket,message){
    if (message && IsJsonString(message)) {
      //var msgObjes=message;
      //ANTIHACKER if(IsJsonString(msgObj)){
      ////consola.log('msgObj',msgObj);
      var msgObj = JSON.parse(message);
      if (msgObj && msgObj[0]) {
        var nombre = msgObj[0];

        if (onsocksvalidos.indexOf(nombre) >= 0) {
          switch(msgObj.length){
            case 1:
              onsocks[nombre](socket);
              break;
            case 2:
              onsocks[nombre](socket,msgObj[1]);
              break;
            case 3:
              onsocks[nombre](socket,msgObj[1],msgObj[2]);
              break;
            case 4:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3]);
              break;
            case 5:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4]);
              break;
            case 6:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5]);
              break;
            case 7:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6]);
              break;
            case 8:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7]);
              break;
            case 9:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8]);
              break;
            case 10:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9]);
              break;
            case 11:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10]);
              break;
          }
        }
        //consola.log('message',msgObj);
      }
    }
  }

  // Initialise WSS
  // passing or reference to web server so WS would knew port and SSL capabilities
  var wss = new WebSocketServer({server: app});
  var client_id = 0;
  wss.on('connection', function (socket) {
    client_id++;
    socket.id = client_id;
    socket.request = {}
    console.log('hola socket');
    socket.on('message', function (message) { onmensaje(socket,message); });
  });

  // console.log(wss);

  //START SOCKETS
  socket_on('login', function(socket, emailuser, passuser) {
    var query = {email: emailuser, pass: passuser};
    dbo.collection("clients").find(query).toArray(function(err, result) {
      if (err) throw err;
      console.log(result);

      if (result.length == 0) {
        // bad login
        socket_emit(socket,'login_result','badlogin');
      } else {
        // login ok

        // result.forEach(function (column) {
        // console.log("%s\t%s", column.metadata.colName, column.value);
        // });

        //set login vars in login to socket
        socket.request = result[0];
        socket_emit(socket,'login_result',result);
      }
    });
    console.log('socket_OnLOGIN',emailuser,passuser);
  });

  // DATA FETCH ROUTES
  socket_on('fetch:userLeads', function (socket) {
    var query = '';
    dbo.collection("clients").find(query).toArray(function(err, result) {
      if (err) throw err;
      socket_emit(socket, 'respond:$userID', result);
    });
  });

  socket_on('fetch:userProfile', function (socket) {
    dbo.someRequest(function(err, result) {
      if (err) throw err;
      socket_emit(socket, 'respond:$userID', result);
    });
  });

  socket_on('fetch:userSettings', function (socket) {
    dbo.someRequest(function(err, result) {
      if (err) throw err;
      socket_emit(socket, 'respond:$userID', result);
    });
  });

  // DATA UPDATE ROUTES
  socket_on('update:userLeads', function (socket) {
    // update db, send confirmation response
    // to updater, send datasync to other clients

    let response; // success or failure
    socket_emit(socket, 'respond:$userID', response) // respond to updater
  })


  //END SOCKETS

});
//END MONGODB SQL
