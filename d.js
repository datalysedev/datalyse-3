//START WEBSERVER DATALYSE
var express = require('express');
var fs = require('fs');
var app = express();

app.use(express.static('website'));

app.get('/', function (req, res) {
   res.sendFile('index.html');
});

app.get(/^(.+)$/, function(req, res){
    //if (fs.existsSync(req.params[0])) {
    //    res.sendFile(req.params[0]);
    //}else{
        res.sendfile('website/index.html')
    //}
});

app.listen("80","93.93.68.140", function () {
  console.log('Example app listening on port 80!');
});
//END WEBSERVER DATALYSE

//START MONGODB SQL

var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
// Connect to the db
MongoClient.connect("mongodb://localhost:27017/datalysedb", function(err, db) {
  var dbo = db.db("datalysedb");
  if (err) {
    throw err;
  }
  console.log("We are connected");
    //INIT start websockets and more process if never started after

  //START SOCKETS
  var WebSocketServer = require('ws').Server;
  var cfg = {
    ssl: true,
    port: 8222,
    ssl_key: '/furanet/sites/app.datalyse.co/web/certificate.key',
    ssl_cert: '/furanet/sites/app.datalyse.co/web/certificate.crt'
  };
  var httpServ = (cfg.ssl) ? require('https') : require('http');
  var app = null;
  var processRequest = function(req, res) {
    res.writeHead(200);
    res.end("Enter to datalyse.co\n");
  };

  if (cfg.ssl) {
    app = httpServ.createServer({
        key: fs.readFileSync(cfg.ssl_key),
        cert: fs.readFileSync(cfg.ssl_cert)
      }, processRequest).listen(cfg.port);
  } else {
    app = httpServ.createServer(processRequest).listen(cfg.port);
  }

  var onsocks = {};
  var onsocksvalidos = [];
  function socket_on(nombre,callback) {
    onsocksvalidos.push(nombre);
    onsocks[nombre] = callback;
  };

  function socket_emit() {
    var datas = [];
    var sockete=arguments[0];
    if (sockete) {
      datas.push(arguments[1]);
      if (arguments.length>1) {
        for (var i=2; i<arguments.length; i++) {
          datas.push(arguments[i]);
        }
      }
      if (sockete && sockete.readyState && sockete.readyState == 1) {
        sockete.send(JSON.stringify(datas));
      } //abierto
    }
  };

  function IsJsonString(str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  }

  function onmensaje(socket,message){
    if (message && IsJsonString(message)) {
      //var msgObjes=message;
      //ANTIHACKER if(IsJsonString(msgObj)){
      ////consola.log('msgObj',msgObj);
      var msgObj = JSON.parse(message);
      if (msgObj && msgObj[0]) {
        var nombre = msgObj[0];

        if (onsocksvalidos.indexOf(nombre) >= 0) {
          switch(msgObj.length){
            case 1:
              onsocks[nombre](socket);
              break;
            case 2:
              onsocks[nombre](socket,msgObj[1]);
              break;
            case 3:
              onsocks[nombre](socket,msgObj[1],msgObj[2]);
              break;
            case 4:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3]);
              break;
            case 5:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4]);
              break;
            case 6:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5]);
              break;
            case 7:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6]);
              break;
            case 8:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7]);
              break;
            case 9:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8]);
              break;
            case 10:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9]);
              break;
            case 11:
              onsocks[nombre](socket,msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10]);
              break;
          }
        }
        //consola.log('message',msgObj);
      }
    }
  }

  // passing or reference to web server so WS would knew port and SSL capabilities
  var wss = new WebSocketServer({server: app});
  var client_id = 0;
  wss.on('connection', function (socket) {

    var ip_este = 'nose';
    if(socket && socket._socket && socket._socket.remoteAddress){
    ip_este = socket._socket.remoteAddress;
    ip_este = ip_este.replace('::ffff:','');
    }

    console.log('IPSOCKET: >'+ip_este+'<');
    client_id++;
    socket.id = client_id;
    socket.request = {};
    //var address = socket.handshake.address;
    //console.log("Ip from " + address.address + ":" + address.port);

    //console.log('hola socket');
    socket.on('message', function (message) { onmensaje(socket,message); });
  });


  //START SOCKETS
  socket_on('login', function(socket, emailuser, passuser) {
    var query = {email: emailuser, pass: passuser};
    dbo.collection("clients").find(query).toArray(function(err, result) {
      if (err) throw err;
      //console.log(result);

      if (result.length == 0) {
        // bad login
        socket_emit(socket,'login_result','badlogin');
      } else {
        // login ok

        // result.forEach(function (column) {
        // console.log("%s\t%s", column.metadata.colName, column.value);
        // });

        //set login vars in login to socket
        socket.request = result[0];
        socket_emit(socket,'login_result',result[0]);
      }
    });
    console.log('socket_OnLOGIN',emailuser,passuser);
  });

    //MARCMODULE CONTROL START

  socket_on('control_getAgents', function (socket,datas) {

  if(socket.request && socket.request.company_id){
      var agents = [];
  const stream = dbo.collection('clients').find({
    company_id : socket.request.company_id
  }).toArray(async function(err, result) {
    if (err) {
      throw err;
    }
    
    // result.map - Loop a trav�s de los resultados de la db
    // devolviendo "Promise" a agents.
    // Cuando se termine de ejecutar tendremos:
    //  agents = [ Promise, Promise ]
    // Son funciones as�ncronas por lo que el siguiente paso es resolverlas.
    var agents = result.map(async (agent, index) => {
      const leads = await dbo.collection('leads').find({
        'agent_id': String(agent._id),
        'status': 'New Lead',
      }).count();
      
      return {
        _id: agent._id,
        name: agent.name,
        lastname: agent.lastname,
        type: agent.type,
        country: agent.country,
        email: agent.email,
        total_leads: leads,
      };
    });
    
    // Promise.all - Espera a que todos los items del array "agents"
    // se resuelvan y devuelve sus valores.
      socket_emit(socket, 'control_getAgentsRESP', await Promise.all(agents));
  });
  }
  });

  socket_on('control_getLeads', async function (socket,datas) {

  if(socket.request && socket.request.company_id)
  {
    var query = {company_id : socket.request.company_id};
    if(datas.control_filter_name){
    query.name = new RegExp(datas.control_filter_name, 'i');
    }
    if(datas.control_filter_email){
    query.email = new RegExp(datas.control_filter_email, 'i');
    }
    
    const total_leads = await dbo.collection('leads').find(query).count();
    
    console.log('total_leads',total_leads);
    
    var var_resultspage = datas.var_resultspage; //number of leads to show in page
    var var_page = datas.var_page; //page number
    
    var startinresult=0;
    if(var_page>1)
    startinresult=(var_page-1)*var_resultspage;
    
        dbo.collection("leads").find(query).skip(startinresult).limit(var_resultspage).toArray(function(err, result) {
          if (err) throw err;
          socket_emit(socket, 'control_getLeadsRESP', result,total_leads);
        });
  }
  });

  socket_on('control_changeleads', function (socket,action,idsleadsarray,newagentorstatus) {

  if(socket.request && socket.request.company_id && action && idsleadsarray && Array.isArray(idsleadsarray)){

    console.log('isarray:',idsleadsarray);

    for(var i in idsleadsarray){
      var idleadeste = idsleadsarray[i];
      var myquery = {_id : ObjectId(idleadeste),company_id : socket.request.company_id};

        var newvalues;
      if (action == 'setagent') {
        newvalues = { $set: {agent_id: newagentorstatus } };
      } else if (action == 'changestatus') {
        newvalues = { $set: {status: newagentorstatus } };
      }
    if(newvalues){
    console.log('hay new values');
    dbo.collection("leads").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log("1 document updated");
    });
    }

    }
    socket_emit(socket,'control_changeleadsOK');
  }
  });

  socket_on('profile_getAgent', function (socket) {
    if (socket.request && socket.request._id) {
      var myquery = {_id: socket.request._id}
      dbo.collection("clients").findOne(myquery, function (err, result) {
          if (err) throw err;
          socket_emit(socket, 'profile_getAgentRESP', result);
      })
    }
  });

  socket_on('profile_updateClient', async function (socket,name,lastname,email,phone,birth) {
    if (socket.request && socket.request._id){
        
    
    const checkemail = await dbo.collection('clients').find({email: email}).count();
    
    if(email != socket.request.email && checkemail == 1){
    //dont change email exists in other client account
    
        dbo.collection("clients").updateOne({_id: ObjectId(socket.request._id)}, {$set: {name:name,lastname:lastname,phone:phone,birth:birth}}, 
        function(err,res){
        
        if(!err && socket){
        socket.request.name=name;
        socket.request.lastname=lastname;
        socket.request.phone=phone;
        socket.request.birth=birth;
    socket_emit(socket, 'profile_updateClientOK','emailexits',email);
    
        }
        });
        
    }else{
    
        dbo.collection("clients").updateOne({_id: ObjectId(socket.request._id)}, {$set: {name:name,lastname:lastname,email:email,phone:phone,birth:birth}}, 
        function(err,res){
          
        if(!err && socket){
        socket.request.name=name;
        socket.request.lastname=lastname;
        socket.request.phone=phone;
        socket.request.birth=birth;
        socket.request.email=email;
          socket_emit(socket, 'profile_updateClientOK',err,res);
        }
        });
    }
    }
  });
  
  socket_on('profile_updatePass', function (socket,pass) {
    if (socket.request && socket.request._id){
        
        dbo.collection("clients").updateOne({_id: ObjectId(socket.request._id)}, {$set: {pass:pass}}, function(err, res) {
        if(!err && socket){
        socket.request.pass=pass;
          socket_emit(socket, 'profile_updatePassOK',err,res);
          }
        });
    }
  });

    //MARCMODULE CONTROL END






  // DATA FETCH ROUTES
//  socket_on('fetch:userLeads', function (socket) {
//    var query = '';
//    dbo.collection("clients").find(query).toArray(function(err, result) {
//      if (err) throw err;
//      socket_emit(socket, 'respond:$userID', result);
//    });
//  });
//
//  socket_on('fetch:userProfile', function (socket) {
//    dbo.someRequest(function(err, result) {
//      if (err) throw err;
//      socket_emit(socket, 'respond:$userID', result);
//    });
//  });
//
//  socket_on('fetch:userSettings', function (socket) {
//    dbo.someRequest(function(err, result) {
//      if (err) throw err;
//      socket_emit(socket, 'respond:$userID', result);
//    });
//  });
//
//  // DATA UPDATE ROUTES
//  socket_on('update:userLeads', function (socket) {
//    let response; // success or failure
//    socket_emit(socket, 'respond:$userID', response) // respond to updater
//  })


  //END SOCKETS

});
//END MONGODB SQL
