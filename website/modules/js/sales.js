kajax({
  url: '/components/d3.js',
  data: '',
  type: 'get',
  success: function () {
    setTimeout(init(), 500);
  }
});

// page constants
var salesData;

function init () {
  console.log('run sales script');
  console.log(d3);

  // 1. load data from mongodb
  salesData = loadLeads();
  console.log('Sales Data:', salesData);
}

function loadLeads () {
  var sampleLeads = [
    {
      customerID: 1,
      profileForename: 'Tim',
      profileSurname: 'OConnell'
    },
    {
      customerID: 2,
      profileForename: 'Marc',
      profileSurname: 'Spanucci'
    },
    {
      customerID: 3,
      profileForename: 'Ben',
      profileSurname: 'Bopkins'
    },
    {
      customerID: 4,
      profileForename: 'Rio',
      profileSurname: 'Karimo'
    }
  ];

  var leads;
  socket_on('response:leads', function (response) {
    if (response) {
      console.log('true', response);
      leads = response;
    } else {
      console.log('false', response);
      leads = sampleLeads;
    }
    return leads;
  });

  socket_emit('fetch:leads');
}

function generateLeadList (leads) {
  leads.forEach(function (leads) {

  })
}

function generateLeadListEntry (lead) {
  var el = "<div class='list-lead'>" +
      "<div class='list-lead-icon'></div>" +
      "<div class='list-lead-name'></div>" +
      "<div class='list-lead-created'></div>" +
      "</div>";
  el.querySelector('.list-lead-name').innerHTML = lead.profileForename;
}