App.addBehavior('tooltip', function () {
  var TOOLTIP_PREFIX = 'data-tooltip';
  var TOOLTIP_HTML = '<div id="tooltip" class="tooltip is-top-aligned" role="alert"><div class="tooltip__inner">{text}</div></div>';
  
  var tooltipCache = {};
  var numTooltips = 0;
  var scrollEventAdded = false;
  
  function getTarget(evt) {
    var target = evt.target || evt.srcElement;
    if (!target) {
      return null;
    }
    if (target.getAttribute(TOOLTIP_PREFIX) === "") {
      return target;
    }
    while (true) {
      target = target.parentNode;
      if (!target || target === document.body || !target.getAttribute) {
        return null;
      } else if (target.getAttribute(TOOLTIP_PREFIX) === "") {
        return target;
      }
    }
  }
  
  function registerTooltip(targetEl) {
    targetEl.addEventListener('mouseenter', function(evt) {
      var target = getTarget(evt);
      showTooltip(target);
    });
    targetEl.addEventListener('mouseleave', function(evt) {
      var target = getTarget(evt);
      hideTooltip(target);
    });
  }
  
  function initializeHideOnScroll() {
    if (window.addEventListener) {
      window.addEventListener('scroll', hideAllTooltips, true);
    }
    scrollEventAdded = true;
  }
  
  function showTooltip(target) {
    var text = target.getAttribute('aria-label');
    
    var el = document.createElement('div');
    el.innerHTML = TOOLTIP_HTML.replace('{text}', text);
    
    var tooltipId = generateUID('tooltip');
    var tooltipEl = el.firstChild;
    tooltipEl.setAttribute('id', tooltipId);
    document.body.append(tooltipEl);
    
    target.setAttribute('aria-describedby', tooltipId);
    
    tooltipCache[tooltipId] = tooltipEl;
    numTooltips++;
    
    updateTooltipPosition(target, tooltipEl);
    
    if (!scrollEventAdded) {
      initializeHideOnScroll();
    }
  }
  
  function generateUID(prefix) {
    do {
      prefix += Math.floor((Math.random() * 1000000));
    } while (document.getElementById(prefix));
    
    return prefix;
  }
  
  function hideTooltip(target) {
    var tooltipId = target.getAttribute('aria-describedby');
    var tooltipEl = tooltipCache[tooltipId];
    if (tooltipEl) {
      target.removeAttribute('aria-describedby');
      tooltipEl.remove();
      delete tooltipCache[tooltipId];
      numTooltips--;            
    }
  }
  
  function hideAllTooltips() {
    var tooltips = document.querySelectorAll('.tooltip');
    for (var i = 0; i < tooltips.length; i++) {
      tooltips[i].remove();                
    }
  }
  
  function updateTooltipPosition(targetEl, tooltipEl) {
    var targetBounds = targetEl.getBoundingClientRect();
    var tooltipBounds = tooltipEl.getBoundingClientRect();
    
    var targetMiddle = (targetBounds.width - tooltipBounds.width) / 2;
    
    var margin = 43;
    
    tooltipEl.style.top = (targetBounds.y - margin) + 'px';
    tooltipEl.style.left = (targetBounds.x + targetMiddle) + 'px';
  }
  
  // Just cleaning the DOM
  // before registering new ones.
  hideAllTooltips();
  
  var tooltips = document.querySelectorAll('[' + TOOLTIP_PREFIX + ']');
  for (var i = 0; i < tooltips.length; i++) {
    registerTooltip(tooltips[i]);
  }
});
