App.addBehavior('phone', function() {
  var TOGGLE_BUTTON = 'phone-toggle-button';
  
  function Phone() {
    this.element = document.querySelector('[data-type="' + TOGGLE_BUTTON + '"]');
    this.element.addEventListener('click', this.toggleHandler.bind(this));
  }
  
  Phone.prototype.toggleHandler = function() {
    this.element.parentNode.classList.toggle('phone--opened');
  };
  
  return new Phone();
});
