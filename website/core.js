var global_datauser = {};
//CONFIGS list available status for leads
var core_liststatus = {
    'New Lead':'New Lead',
    'No answer':'No answer',
    'Wrong Info':'Wrong Info',
}

//DALYSE START clientLIB socket
var connected = false;
var isfirstconection='yes';
var onsocks=[],sokinew,socket_emit = function(){if(connected==true){var datas = [];datas.push(arguments[0]);if(arguments.length>0){for(var i=1; i<arguments.length; i++){datas.push(arguments[i]);}}sokinew.send(JSON.stringify(datas));}},
onmensa = function(event){var msgObj = JSON.parse(event.data);var nombre = msgObj[0];switch(msgObj.length){case 1:onsocks[nombre]();break;case 2:onsocks[nombre](msgObj[1]);break;case 3:onsocks[nombre](msgObj[1],msgObj[2]);break;case 4:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3]);break;case 5:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4]);break;case 6:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5]);break;case 7:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6]);break;case 8:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7]);break;case 9:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8]);break;case 10:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9]);break;case 11:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10]);break;case 12:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10],msgObj[11]);break;case 13:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10],msgObj[11],msgObj[12]);break;case 14:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10],msgObj[11],msgObj[12],msgObj[13]);break;case 15:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10],msgObj[11],msgObj[12],msgObj[13],msgObj[14]);break;case 16:onsocks[nombre](msgObj[1],msgObj[2],msgObj[3],msgObj[4],msgObj[5],msgObj[6],msgObj[7],msgObj[8],msgObj[9],msgObj[10],msgObj[11],msgObj[12],msgObj[13],msgObj[14],msgObj[15]);break;}},
socket_on = function(nombre,callback){onsocks[nombre] = callback;}
//DALYSE end clientLIB socket

//START DATALYSE client socket
var onloadpage = function(){
  App.startBehaviors();
    console.log('onloadpage');
if (location.protocol != 'http:')
{
    console.log('location no es https');
 location.href = 'http:' + window.location.href.substring(window.location.protocol.length);
}else{
    console.log('socket https');
    sokinew = new WebSocket("ws://localhost:8000");
       sokinew.onopen = function(e){
       console.log('onopen',e);
       connected=true;
        sokinew.onmessage= onmensa;
            //console.log('onopen');
            showlogin()
    };
    sokinew.onclose = function(e)
    {
    connected = false;
    console.log('onclose',e);
    if(isfirstconection == 'logout'){
      document.querySelector('.errorlogin').innerHTML='Current session is closed';
    isfirstconection='yes';
    onloadpage();
    }else{
    isfirstconection='no';
    
        document.querySelector('.js-login').setAttribute('aria-hidden', true);
        document.querySelector('.js-sidebar').setAttribute('aria-hidden', true);
        //show spinner
        document.querySelector('.js-module-contents').classList.remove('invisible');
        document.querySelector('.js-module-contents').innerHTML='<div class="module-state"><div class="module-spinner">lost the connection, trying to connect. Please wait ..<div></div><div></div><div></div></div></div>';
        document.querySelector('.js-phone').classList.add('u-hidden');
        setTimeout(onloadpage,1000);
        
        
function updateClock() {
    var now = new Date(), // current date
        months = ['January', 'February', 'March', 'April', 'May',
         'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        time = now.getHours() + ':' + now.getMinutes(),
        date = [now.getDate(), 
                months[now.getMonth()],
                now.getFullYear()].join(' ');
    document.querySelector('.module__clock').innerHTML = [date, time].join(' | ');
    setTimeout(updateClock, 1000);
}
// updateClock();



    }
    };
sokinew.onerror = function(e){
connected = false;
console.log('onerror',e);
      document.querySelector('.errorlogin').innerHTML='Connection error';
    showlogin()
};

}}

//END DATALYSE client socket

function showlogin(){
    console.log('showlogin');
  document.querySelector('.js-login').setAttribute('aria-hidden', false);
  document.querySelector('.js-sidebar').setAttribute('aria-hidden', true);
  
  var loginForm = document.getElementById('login_form');
  loginForm.onsubmit = function(evt) {
    evt.preventDefault();
    
    loginnow();
  };
  
  if(isfirstconection=='no')
  loginnow();
}

function logout(){
isfirstconection='logout';
sokinew.close();
}

window.onload = onloadpage;

function loginnow(){

  var loginForm = document.getElementById('login_form');
    var emailField = loginForm.querySelector('#login_field_email');
    var passwordField = loginForm.querySelector('#login_field_password');
    
    // Handle login response
    socket_on('login_result', function(datauserlogin){
      
      if (datauserlogin == 'badlogin') {
      document.querySelector('.errorlogin').innerHTML='Email or password incorrect';
        showlogin();
      } else {
        global_datauser = datauserlogin;
        document.querySelector('.js-login').setAttribute('aria-hidden', true);
        document.querySelector('.js-sidebar').setAttribute('aria-hidden', false);
        document.querySelector('.js-module-contents').classList.remove('invisible');
        document.querySelector('.js-phone').classList.remove('u-hidden');
        document.querySelector('.js-phone-body').innerHTML = '<iframe id="webphoneiframe" src="/webphone/?webphone_user='+global_datauser.webphone_user+'&webphone_pass='+global_datauser.webphone_pass+'&webphone_callerid='+global_datauser.webphone_callerid+'&webphone_lang='+global_datauser.language+'" width="100%" height="100%" frameborder="0"></iframe>';
        
        var currentmoduletoload = window.location.pathname;
        loadmodule(currentmoduletoload);
      }
    });
    
    // Send login details
    socket_emit('login', emailField.value, passwordField.value);
}
function hashchange(){
var url = location.hash;//.slice(2) || 'pacasa/'

    //console.log('hashchange',url);
}

function tourl(cual){
        ////console.log('setloc');
        try {
        //console.log('historypush cual',cual);
          window.history.pushState({setlocvoy:cual}, '', '/' +cual);
          loadmodule(cual);
          return;
        } catch(e) {}
      window.chHashFlag = true;
      var pondreha='#' + '/' + cual;
        //console.log('HASH cual',cual);
      if(location.hash == pondreha){
        //console.log('HASH igual',cual);
      loadmodule(cual);
      }
      location.hash = pondreha;
}
function volviatras(event) {
var adondevoy='';
var miroeve;
if(event.state){
miroeve= event.state;
adondevoy=miroeve.setlocvoy;
}
  //console.log("volviatras adondevoy: " + adondevoy);
  loadmodule(adondevoy);
}

function kajax(a){
    var xmlhttp;


if(a.type == 'get' && a.data){
a.url+='?'+a.data;
}
    if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {
        // code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 ) {
           if(xmlhttp.status == 200){
            a.success(xmlhttp.responseText);
           }
           else if(xmlhttp.status == 400) {
           setTimeout(kajax,1000,a);
           }
           else {
           setTimeout(kajax,1000,a);
           }
        }
    }

if(a.type == 'get' || a.type == 'GET'){
    xmlhttp.open("GET", a.url, true);
    xmlhttp.send();
}else{
    xmlhttp.open("POST", a.url, true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send(a.data);
}
}

function bindelements(module) {
  App.startBehavior('tooltip');
}

function checkscripts(content) {
  var scripts = content.getElementsByTagName('script');
  
  for (var i = 0; i < scripts.length; i++) {
    var script = scripts[i];
    
    var el = document.createElement('script');
    el.setAttribute('type', 'text/javascript');
    if (script.src.length > 0) {
      el.setAttribute('crossorigin', 'anonymous');
      el.src = script.src;
    } else {
      el.innerHTML = script.innerHTML;
    }
    
    script.remove();
    content.appendChild(el);
  }
}

function loadmodule(module){
var modulepage = module;

if(modulepage.substr(0,1) == '/')
modulepage = modulepage.substr(1);


if(modulepage.substr((modulepage.length-1),modulepage.length) == '/')
modulepage = modulepage.substr(0,(modulepage.length-1));


if(modulepage == '' || modulepage == '/')modulepage='home';

modulepage = modulepage.replace('/','-');
//console.log('loadmodule modulepage',modulepage);
      kajax({
    url:'/modules/'+modulepage+'.html',
    data:'',
    type:'get', 
    success:function(devuel){
      var moduleContents = document.querySelector('.js-module-contents');
      moduleContents.innerHTML = devuel;
      checkscripts(moduleContents);
      bindelements(moduleContents);
    }});
}
function alerta(data){
alert(data.msg);
}

function callphone(tel){
var typesip='phone';
var phonetocall=tel;//34682288834
var from=global_datauser.webphone_user;//USER FROM SIP FOR WEBPHONE
var global_password=global_datauser.webphone_pass;//pass sip
var global_lang=global_datauser.language;
var global_displayname=global_datauser.webphone_callerid;//callerid
kajax({
url: 'https://spanads.com/e14/incs/apivoip.php',
data: 'op=calldesdewebphone&to='+typesip+'_'+phonetocall+'&from='+from+'&lang='+global_lang+'&pass='+global_password+'&callernumber='+global_displayname,
type: 'get',
success: function(){

console.log('mando llamada aqui');
}
});
}
window.addEventListener('popstate', volviatras, false);
window.addEventListener('hashchange', hashchange, false);