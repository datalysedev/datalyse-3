window.App = window.App || {};

var behaviors = {};

/**
 * Pushes a behavior to the behavior list
 * @usage App.addBehavior('my-behavior', myBehaviorFunction);
 */
App.addBehavior = function(behaviorName, creator) {
  if (typeof behaviors[behaviorName] !== 'undefined') {
    console.error(new Error('Behavior ' + behaviorName + ' has already been added.'));
    return this;
  }
  behaviors[behaviorName] = {
    creator: creator,
    instance: null
  };
  return this;
};

/**
 * Starts all behaviors on the list
 * @usage App.startBehaviors();
 */
App.startBehaviors = function() {
  for (var behaviorName in behaviors) {
    behaviors[behaviorName].creator();
  }
};

/**
 * Starts a specific behavior
 * @usage App.startBehavior('my-behavior');
 */
App.startBehavior = function(behaviorName) {
  if (typeof behaviors[behaviorName] === 'undefined') {
    console.error(new Error('Behavior ' + behaviorName + ' has not been registered yet.'));
    return this;
  }
  
  behaviors[behaviorName].creator();
}
